import React, { useState } from "react";
import ModalProfile from "./ModalProfile";

const AppBar = () => {
  const [openModalProfile, setOpenModalProfile] = useState(false);

  return (
    <div className="flex flex-row justify-between mb-3">
      <div className="flex flex-row w-[25%] h-[40px]">
        <input
          type="text"
          className="border-2 border-y-slate-400 border-l-slate-400 w-[100%] rounded-l-lg p-2 font-sans"
        />
        <button>
          <img
            src="./assets/Search.png"
            alt=""
            className="border-2 border-slate-400 h-[40px] rounded-r-lg"
          ></img>
        </button>
      </div>
      <div
        className="bg-red-400 rounded-full h-[45px] w-[45px] flex justify-center items-center cursor-pointer relative"
        onClick={() => setOpenModalProfile(!openModalProfile)}
      >
        <div className="text-white">D</div>

        {openModalProfile ? (
          <ModalProfile setOpenModalProfile={setOpenModalProfile} />
        ) : null}
      </div>
    </div>
  );
};

export default AppBar;
